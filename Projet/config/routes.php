<?php

$routes = [
    'home' => 'homeController',
    'about' => 'aboutController',
    'contact' => 'contactController',
    'mentionsLegales' => 'mentionsLegalesController',
    'politiqueconfidentialite' => 'politiqueconfidentialiteController',
    'notFound' => 'notFoundController',
    'dbError' => 'dbErrorController',
    'pageConnection' => 'pageConnectionController',
    'rendu' => 'renduController',
    'profil' => 'profilController',
    'deconnexion' => 'deconnexionController',
    'mdpOublie' => 'mdpOublieController',
    'delRendu' => 'delRenduController',
    'addRendu' => 'addRenduController',
    'reglageAide' => 'reglageAideController',
    'addGroupe' => 'addGroupeController',
    'addUser' => 'addUserController',
    'updRendu' => 'updRenduController'
];