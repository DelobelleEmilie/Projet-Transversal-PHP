<?php

require_once '../vendor/autoload.php';
require_once '../config/routes.php';
require_once '../config/config.php';
require_once '../src/router.php';
require_once '../src/twig.php';
require_once '../src/database.php';
require_once '../src/model/authentification.php';

$db = getConnection($config);

session_start();


$auth = 0;

if (!isset($_SESSION['username'])) {
    $auth = Authentification($db);
}else {
    $auth = "connected";
}


//Init de Twig
$twig = initTwig('../template/');


//Recup la route suivant la page
$actionController = initRouter($routes, $db, $auth);

//Affiche la page
//if ($auth) {
$actionController($twig, $db);
//}

//Crypter la bdd