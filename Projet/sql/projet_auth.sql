-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : sam. 21 jan. 2023 à 16:32
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `projet_auth`
--

-- --------------------------------------------------------

--
-- Structure de la table `boite_rendu`
--

DROP TABLE IF EXISTS `boite_rendu`;
CREATE TABLE IF NOT EXISTS `boite_rendu` (
  `idboiterendu` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `idgroupe` int(11) NOT NULL,
  `prof` varchar(25) NOT NULL,
  `datecreation` datetime NOT NULL,
  `dateline` datetime NOT NULL,
  `estOuvert` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idboiterendu`),
  KEY `idgroupe` (`idgroupe`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `boite_rendu`
--

INSERT INTO `boite_rendu` (`idboiterendu`, `label`, `description`, `idgroupe`, `prof`, `datecreation`, `dateline`, `estOuvert`) VALUES
(8, 'TPRE101 - Projet Conception et Développement d\'une solution web', 'Le livrable attendu devra être un fichier zippé contenant tous vos livrables (Présentation Powerpoint et rédactionnel). Le nom du fichier devra indiquer le numéro du groupe et les noms des apprenants constitutifs de ce groupe', 1, 'lfabrice', '2023-01-04 13:13:00', '2023-01-21 03:13:00', 0),
(12, 'Les fondamentaux PHP et MySQL', 'Cours + Exercices Notés', 1, 'lfabrice', '2023-01-15 14:14:00', '2023-01-31 18:18:00', 0),
(13, 'Projet Réalisation GANTT', 'Création d\'un site pour créer son propre GANTT', 2, 'lfabrice', '2023-01-13 14:14:00', '2023-01-16 00:00:00', 0),
(15, 'Culture generale', 'Synthese', 2, 'pstephane', '2023-01-18 19:21:00', '2023-01-20 19:21:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `devoir`
--

DROP TABLE IF EXISTS `devoir`;
CREATE TABLE IF NOT EXISTS `devoir` (
  `iddevoir` int(11) NOT NULL AUTO_INCREMENT,
  `idboiterendu` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `nom_fichier` varchar(255) NOT NULL,
  `chemin_fichier` varchar(255) NOT NULL,
  PRIMARY KEY (`iddevoir`),
  KEY `idboiterendu` (`idboiterendu`),
  KEY `iduser` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `devoir`
--

INSERT INTO `devoir` (`iddevoir`, `idboiterendu`, `iduser`, `nom_fichier`, `chemin_fichier`) VALUES
(16, 13, 1, 'Chapitre 11 - Ajouter et afficher des produits.pdf', 'C:\\Users\\boudr\\Documents\\GitHub\\Projet-Transversal-PHP\\Projet\\src\\controller/../../rendus/13/cparisot/Chapitre 11 - Ajouter et afficher des produits.pdf_cparisot_13.pdf'),
(17, 13, 1, 'Chapitre 12 - Modifier un produit.pdf', 'C:\\Users\\boudr\\Documents\\GitHub\\Projet-Transversal-PHP\\Projet\\src\\controller/../../rendus/13/cparisot/Chapitre 12 - Modifier un produit.pdf_cparisot_13.pdf'),
(20, 12, 1, 'Politique de confidentialité.pdf', 'C:\\Users\\boudr\\Documents\\GitHub\\Projet-Transversal-PHP\\Projet\\src\\controller/../../rendus/12/cparisot/Politique de confidentialité.pdf_cparisot_12.pdf'),
(21, 12, 1, 'epreuve de synthese methodo.pptx', 'C:\\Users\\boudr\\Documents\\GitHub\\Projet-Transversal-PHP\\Projet\\src\\controller/../../rendus/12/cparisot/epreuve de synthese methodo.pptx_cparisot_12.pptx');

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `idgroupe` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`idgroupe`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`idgroupe`, `label`) VALUES
(1, 'SN1'),
(2, 'SN2'),
(3, 'prof');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(100) NOT NULL,
  `mdp` varchar(100) NOT NULL,
  `isAdmin` int(11) NOT NULL,
  `groupe` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `pseudo`, `mdp`, `isAdmin`, `groupe`) VALUES
(1, 'cparisot', '$2y$10$0nex.8BA/4Hwl6qxtBY1IuU/KoIDXJM0t.VDpNn.sKV3YMnw6h.Bu', 0, '1/2/4'),
(2, 'bantoine', '$2y$10$pss9q7VSwZsfXouRZzi8Huv.W3AQy5SPaGEULW1ytPQMAFVrFv94i', 0, '2/4'),
(5, 'lfabrice', '$2y$10$cDBnLmTgoRCaPFnJPqkckO9suZZ6RFt.5qqldbnko8t4mjfVzywz2', 1, '3'),
(6, 'pstephane', '$2y$10$6pTyRI2w/D5HE4wUg7dWS.awXI1Muibc9WHdZwCAT.0yCAhdiWwK6', 1, '3'),
(7, 'bmelanie', '$2y$10$M1iFf4gyIjNzI9QaOgW0aezZp/RlduRdfcbNFjYJE.Sv2a5MIupVG', 0, '0/4'),
(11, 'pagathe', '$2y$10$6hOevYcehwICCbYSs7y6XOv8p5AGTYt7X/PnghbXkAUmIcH33xVI2', 0, '1');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
