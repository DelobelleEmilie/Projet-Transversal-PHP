<?php

function addGroupeController($twig, $db)
{
    include_once '../src/model/RenduModel.php';  #on inclut pour apres

    $erreurAdd = "";
    $erreurDel = "";

    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1){
        #On vérifie que le bouton "btnAddGroupe a été pressé"
        if (isset($_POST['btnAddGroupe'])) {

            //On met tout dans des variables
            $label = htmlspecialchars($_POST['groupeLabel']);
            $users = [];

            //On passe par toutes les cases de POST qui ont "ajout" donc qui sont des pseudos avec checkbox cochées
            //Puis on le retire pour pas le voir deux fois
            while (in_array("ajout", $_POST)) {
                $user = array_search("ajout", $_POST);
                array_push($users, $user);
                $_POST[$user] = null;
            }

            //Et on save
            $erreurAdd = saveGroupe($db, $label, $users);
        } else {
            if (isset($_POST['btnAddGroupe']) == true && isset($_POST['btnDelGroupe']) == false) {
                $erreurAdd = "Veuillez remplir tous les champs";
            }
        }



        if (isset($_POST['btnDelGroupe'])) {

            //On met tout dans des variables
            if (isset($_POST['delGroupe'])) {
                $groupe = $_POST['delGroupe'];
            }

            //Et on supprime
            $erreurDel = delGroupe($db, $groupe);
        }


        echo $twig->render("addGroupe.html.twig", [
            'users' => getAllUsers($db),
            'erreurAdd' => $erreurAdd,
            'erreurDel' => $erreurDel,
            'groupes' => getAllGroupes($db)
        ]);
    }else{
        echo $twig->render("notFound.html.twig", []);
    }
}
