<?php 

function addRenduController($twig,$db){
    include_once '../src/model/RenduModel.php';  ##on inclut pour apres

    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1){
        if((!empty($_POST['renduGroupe'])) && 
            (!empty($_POST['trip-end'])) && 
            (!empty($_POST['trip-start'])) && 
            (!empty($_POST['renduLabel'])) && 
            (!empty($_POST['renduDescription'])) && 
            (isset($_POST['btnAddRendu']))){ 

            $label = htmlspecialchars($_POST['renduLabel']);
            $description = htmlspecialchars($_POST['renduDescription']);
            $idgroupe = htmlspecialchars($_POST['renduGroupe']);
            $datecreation = htmlspecialchars($_POST['trip-start']);
            $dateline = htmlspecialchars($_POST['trip-end']);
            $prof = $_SESSION['username'];

            saveRendu($db,$label,$description,$idgroupe,$prof,$datecreation,$dateline);
        
        }else{
            if(isset($_POST['btnAddRendu'])==true){
                echo '<script language="Javascript">
                    alert ("Veuillez remplir tous les champs" )
                    </script>';
            }
        }
        echo $twig -> render("addRendu.html.twig", [
            'groupes' => getAllGroupes($db)  
        ]);
    }else{
        echo $twig->render("notFound.html.twig", []);
    }
}
