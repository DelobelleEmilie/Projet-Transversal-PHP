<?php

function addUserController($twig, $db)
{
    include_once '../src/model/RenduModel.php';  #on inclut pour apres

    $erreurAdd = "";
    $erreurDel = "";

    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1){
        #On vérifie que le bouton "btnAddGroupe a été pressé"
        if (isset($_POST['btnAddUser'])) {

            //On met tout dans des variables
            $pseudo = htmlspecialchars($_POST['adduserPseudo']);
            $mdp = htmlspecialchars($_POST['adduserMdp']);
            $isAdmin = 0;
            if (isset($_POST['addisAdmin'])) $isAdmin = 1;
            $groupe = $_POST['adduserGroupe'];

            //Et on save
            $erreurAdd = addUser($db, $pseudo, password_hash($mdp, PASSWORD_DEFAULT), $isAdmin, $groupe);
        } else {
            if (isset($_POST['btnAddGroupe']) == true && isset($_POST['btnDelGroupe']) == false) {
                $erreurAdd = "Veuillez remplir tous les champs";
            }
        }


        if (isset($_POST['btnDelUser'])) {

            //On met tout dans des variables
            if (isset($_POST['delUser']) && isset($_POST['delUser'])) {
                $pseudo = $_POST['delUser'];
            }

            //Et on supprime
            $erreurDel = delUser($db, $pseudo);
        }

        echo $twig->render("addUser.html.twig", [
            'users' => getAllUsers($db),
            'erreurAdd' => $erreurAdd,
            'erreurDel' => $erreurDel,
            'groupes' => getAllGroupes($db)
        ]);
    }else{
        echo $twig->render("notFound.html.twig", []);
    }
}
