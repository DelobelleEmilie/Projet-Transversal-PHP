<?php

function deconnexionController($twig)
{
    if (isset($_GET['deconnexion'])) {
        session_destroy();
        echo $twig->render('pageConnection.html.twig');
    }else {
        echo $twig->render('deconnexion.html.twig');
    }
}
