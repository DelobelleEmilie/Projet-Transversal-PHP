<?php 

function delRenduController($twig,$db){
    include_once '../src/model/RenduModel.php';

    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1){
        $oneRendu = getAllRendu($db,$_SESSION['username']);

        if ((isset($_POST['sup'])) && (isset($_POST['btnDelRendu']))){ ##determiner si le tableau est definit btnAddProduct nom du bouton on regarde s'il existe lors d'un envoi de formulaire
            $id=$_POST['sup'];
            delRendu($db,$id);
        }
        echo $twig->render("delRendu.html.twig",[
            'delRendus' => $oneRendu,
            'username' => $_SESSION['username'],
        ]);
    }else{
        echo $twig->render("notFound.html.twig", []);
    }

}

?>