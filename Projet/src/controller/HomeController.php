<?php
include_once "../src/model/RenduModel.php";

function homeController($twig, $db)
{
    $date = date('Y-m-d H:i:s');

    $erreur = "";

    $isAdmin = 0;
    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1) {
        $isAdmin = true;
    } else $isAdmin = false;

    if ($isAdmin) {
        //=========================================ADMIN
        //Si aucun n'est cliqué on ne récup que les classes
        if (!isset($_GET['idparentclasse']) && !isset($_GET['idparentboiterendu'])) {

            $classe = getAllClasse($db);

            echo $twig->render('homeAdmin.html.twig', [
                'listeclasses' => $classe,
                'username' => $_SESSION['username']
            ]);
        }

        //Si que la classe est cliquée on récup ce qu'il y a dans la classe
        if (isset($_GET['idparentclasse']) && !isset($_GET['idparentboiterendu'])) {

            $classe = getAllClasse($db);
            $boiterendu = getAllBoiteRendu($db, $_GET['idparentclasse']);

            #var_dump($boiterendu);

            echo $twig->render('homeAdmin.html.twig', [
                'boiterendus' => $boiterendu,
                'listeclasses' => $classe,
                'username' => $_SESSION['username'],
                'NOW' => $date,
            ]);
        }

        //On va à la fin de l'arborescence
        if (isset($_GET['idparentclasse']) && isset($_GET['idparentboiterendu']) && !isset($_GET['idparentuser'])) {

            $classe = getAllClasse($db);
            $boiterendu = getAllBoiteRendu($db, $_GET['idparentclasse']);
            $users = getAllUsersRendu($db, $_GET['idparentboiterendu']);

            #var_dump($users);

            echo $twig->render('homeAdmin.html.twig', [
                'boiterendus' => $boiterendu,
                'listeclasses' => $classe,
                'listeusers' => $users,
                'username' => $_SESSION['username'],
            ]);
        }

        if (isset($_GET['idparentclasse']) && isset($_GET['idparentboiterendu']) && isset($_GET['idparentuser']) && !isset($_GET['iddevoir'])) {

            $classe = getAllClasse($db);
            $boiterendu = getAllBoiteRendu($db, $_GET['idparentclasse']);
            $users = getAllUsersRendu($db, $_GET['idparentboiterendu']);
            $devoirs = getAllDevoirBoiterendu($db, $_GET['idparentboiterendu']);

            echo $twig->render('homeAdmin.html.twig', [
                'boiterendus' => $boiterendu,
                'listeclasses' => $classe,
                'listeusers' => $users,
                'listedevoirs' => $devoirs,
                'username' => $_SESSION['username'],
            ]);
        }

        if (isset($_GET['idparentclasse']) && isset($_GET['idparentboiterendu']) && isset($_GET['idparentuser']) && isset($_GET['iddevoir'])) {


            // Si le bouton cliqué est 'Télécharger tous les fichiers', on récupere tous les fichiers et on les ajoute à une archive ZIP
            if ($_GET['iddevoir'] == '-1') {
                $user = getOneUserRendu($db, $_GET['idparentuser']);
                $fichiers = getAllDevoirBoiterenduUser($db, $_GET['idparentboiterendu'], $_GET['idparentuser']);
                $fichier_zip = $user['pseudo'] . "_" . $_GET['idparentboiterendu'] . ".zip";

                // On crée l'archive ZIP
                $zip = new ZipArchive();
                $zip->open($fichier_zip, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                // Pour chaque fichier correspondant à la boiterendu et l'utilisateur sur lesquelles nous avons cliqué, on l'ajoute à l'archive ZIP
                foreach ($fichiers as $fichier) {
                    $chemin_fichier = $fichier['chemin_fichier'];
                    $nom_fichier = basename($fichier['chemin_fichier']);
                    $zip->addFile($chemin_fichier, $nom_fichier); // On donne le chemin absolu du fichier et le nom du fichier
                }
                $zip->close();

                // Télécharger le fichier Zip
                header('Content-Type: application/zip');
                header('Content-Disposition: attachment; filename="' . $fichier_zip . '"');
                readfile($fichier_zip);

                // Supprimer le fichier Zip
                unlink($fichier_zip);
            } else {
                $fichier = getOneDevoir($db, $_GET['iddevoir']);
                $chemin_fichier = $fichier['chemin_fichier'];
                $nom_fichier = basename($fichier['chemin_fichier']);
                header('Content-Type: application/octet-stream');
                header('Content-Transfer-Encoding: Binary');
                header('Content-disposition: attachment; filename="' . $nom_fichier . '"');
                readfile($chemin_fichier);
            }
        }
    } else {
        //=========================================ELEVE
        // Upload dans le serveur un fichier
        if (isset($_POST['idboiterendu'])) {
            $idboiterendu = $_POST['idboiterendu'];
            $namesubmit = "ajouterFichierSubmit" . $idboiterendu;
            $namefichier = "fichier" . $idboiterendu;

            if (!empty($_FILES[$namefichier]['tmp_name']) && is_uploaded_file($_FILES[$namefichier]['tmp_name'])) {
                $nomOrigine = $_FILES[$namefichier]['name'];
                $elementsChemin = pathinfo($nomOrigine);
                $extensionFichier = $elementsChemin['extension'];
                $extensionsAutorisees = array("pptx", "ppt", "pptm", "pdf", "zip", "7zip", "rar");
                if (!(in_array($extensionFichier, $extensionsAutorisees))) {
                    //echo '<script>alert("Type de fichier invalide. Uniquement les fichiers Powerpoint (.ppt/.pptx), Zip (.zip) et PDF (.pdf) sont acceptés");</script>';
                    $erreur = "Type de fichier invalide. Uniquement les fichiers Powerpoint (.ppt/.pptx), Zip (.zip) et PDF (.pdf) sont acceptés";
                } else {
                    if (file_exists(dirname(__FILE__) . "/../../rendus/" . $_POST['idboiterendu'] . "/")) {
                        //echo '<script>alert("Le dossier devoir existe dèja");</script>';
                    } else {
                        mkdir(dirname(__FILE__) . "/../../rendus/" . $_POST['idboiterendu'] . "/");
                        //echo '<script>alert("Le dossier devoir à été crée!.");</script>';
                    }
                    // Check pour voir si le dossier de l'utilisateur existe ($repertoireDestination), si il n'existe pas on le crée
                    $repertoireDestination = dirname(__FILE__) . "/../../rendus/" . $_POST['idboiterendu'] . "/" . $_SESSION['username'] . "/";
                    if (file_exists($repertoireDestination)) {
                        //echo '<script>alert("Le dossier utilisateur existe dèja");</script>';
                    } else {
                        mkdir($repertoireDestination);
                        //echo '<script>alert("Le dossier utilisateur à été crée!.");</script>';
                    }
                    $nomDestination = $nomOrigine . "_" . $_SESSION['username'] . "_" . $idboiterendu . "." . $extensionFichier;

                    if (move_uploaded_file($_FILES[$namefichier]["tmp_name"], $repertoireDestination . $nomDestination)) {
                        //echo '<script>alert("Fichier Uploader avec succés!");</script>';
                        addFichierRendu($db, $idboiterendu, $_SESSION['iduser'], $elementsChemin['basename'], $repertoireDestination . $nomDestination);
                        //echo '<script>alert("Fichier ajouté dans la base de donnée !");</script>';
                        $erreur = "Fichier rendu avec succès !";
                    } else {
                        //echo '<script>alert("Probléme lors de la procédure");</script>';
                    }
                }
            }
        }
        if (isset($_POST['supprimerFichierID']) && isset($_POST['supprimerFichierCHEMIN'])) {
            $chemin_fichier_supprimer = $_POST['supprimerFichierCHEMIN'];
            unlink($chemin_fichier_supprimer);
            delFichier($db, $_POST['supprimerFichierID']);
        }

        $rendueleveouvert = AfficherRenduEleveOuvert($db, $_SESSION['username']);
        //var_dump($rendueleveouvert);

        $rendueleveferme = AfficherRenduEleveFerme($db, $_SESSION['username']);
        //var_dump($rendueleveferme);


        echo $twig->render('homeUser.html.twig', [
            'username' => $_SESSION['username'],
            'rendueleveouvert' => $rendueleveouvert,
            'rendueleveferme' => $rendueleveferme,
            'fichiersrendus' => getFichiersRendus($db),
            'erreur' => $erreur
        ]);
    }
}
