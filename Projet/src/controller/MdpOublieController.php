<?php

include_once "../src/model/authentification.php";

function mdpOublieController($twig,$db){
    if((isset($_POST['modifier'])) && (isset($_POST['username'])) && (isset($_POST['newpassword'])) && (htmlspecialchars($_POST['newpassword']) == htmlspecialchars($_POST['confnewpassword']))){
        $username = htmlspecialchars($_POST['username']);
        $iduser = getiduser($db,$username);
        $newpassword = htmlspecialchars($_POST['newpassword']); ##htmlspecialchars eviter d'executer du cote malvaillant
        updatemdp($db,$iduser,password_hash($newpassword, PASSWORD_DEFAULT));
        echo $twig->render("pageConnection.html.twig",[]);
    }
    
    if((isset($_POST['modifier'])) && (isset($_POST['username'])) && (isset($_POST['newpassword'])) && (htmlspecialchars($_POST['newpassword']) != htmlspecialchars($_POST['confnewpassword']))){
        $_POST["passwdError"] = true;
        echo $twig->render("mdpOublie.html.twig",['passwdError' => saisieError()]);
    }

    if ((!isset($_POST['modifier']))){
        echo $twig->render("mdpOublie.html.twig",[]);
    }
}

function saisieError() {
    if (isset($_POST['passwdError'])){
        if ($_POST['passwdError']){
            return true;
        }
    } else return false;
}
?>