<?php

include_once "../src/model/authentification.php";

function pageConnectionController($twig,$db){
    if (isset($_POST['mdp']) && (isset($_POST['username']))){
        $user = getcountIdUser($db,$_POST['username']);
        if($user[0] == 0){
            $_POST["userError"] = true;
            echo $twig -> render('pageConnection.html.twig', [ 
                'userError' => getUserError()]);
        }
        else{
            echo $twig -> render('mdpOublie.html.twig', []);
        }
    }
    if (!isset($_POST['mdp'])){
        echo $twig -> render('pageConnection.html.twig', [
        'mdpError' => getMdpError()
        ]);
    }
}

function getMdpError() {
    if (isset($_POST['mdpError'])){
        if ($_POST['mdpError']){
            return true;
        }
    } else return false;
}

function getUserError() {
    if (isset($_POST['userError'])){
        if ($_POST['userError']){
            return true;
        }
    } else return false;
}