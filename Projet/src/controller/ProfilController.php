<?php

include_once "../src/model/authentification.php";
include_once "../src/model/RenduModel.php";


function profilController($twig, $db)
{
    $isAdmin = 0;
    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1) {
        $isAdmin = true;
    } else $isAdmin = false;

    if ((isset($_POST['modifiermdp'])) && (isset($_POST['actupassword'])) && (isset($_POST['confnewpassword'])) && (isset($_POST['newpassword']))) {
        $actupassword = htmlspecialchars($_POST['actupassword']);
        if ((password_verify($actupassword, $_SESSION['password'])) && (htmlspecialchars($_POST['confnewpassword']) == htmlspecialchars($_POST['newpassword']))) {
            $username = $_SESSION['username'];
            $iduser = getiduser($db, $username);
            $newpassword = htmlspecialchars($_POST['newpassword']);
            $_SESSION['password'] = password_hash($newpassword, PASSWORD_DEFAULT); ##htmlspecialchars eviter d'executer du cote malvaillant
            updatemdp($db, $iduser, $_SESSION['password']);

            header("Location: index.php?page=profil");
        } else {
            $_POST["saisiePasswd"] = true;

            echo $twig->render("profil.html.twig", [
                'username' => $_SESSION['username'],
            'groupes' => getAllGroupes($db),
            'userGroupes' => SQLArrayToArray(getUserGroupeFromPseudo($db, $_SESSION['username']))
            ]);
        }
    }

    if (!isset($_POST['modifiermdp'])) {

        echo $twig->render("profil.html.twig", [
            'username' => $_SESSION['username'],
            'groupes' => getAllGroupes($db),
            'userGroupes' => SQLArrayToArray(getUserGroupeFromPseudo($db, $_SESSION['username']))
            
        ]);
    }
}

function getErrorSaisiePasswd()
{
    if (isset($_POST['saisiePasswd'])) {
        if ($_POST['saisiePasswd']) {
            return true;
        }
    } else return false;
}
