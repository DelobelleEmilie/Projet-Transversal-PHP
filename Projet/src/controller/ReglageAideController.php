<?php

function reglageAideController($twig) {
    $isAdmin = 0;
    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1) {
        $isAdmin = true;
    } else $isAdmin = false;

    echo $twig -> render('reglageAide.html.twig', ["isAdmin" => $isAdmin]);
}

