<?php 

function updRenduController($twig,$db){
    include_once '../src/model/RenduModel.php';  ##on inclut pour apres

    if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1){

        if((isset($_POST['renduGroupe'])) && 
        (isset($_POST['trip-end'])) && 
        (isset($_POST['trip-start'])) && 
        (isset($_POST['renduLabel'])) && 
        (isset($_POST['renduDescription'])) && 
        (isset($_POST['btnUpdRendu']))){

            if((!empty($_POST['renduGroupe'])) && 
            (!empty($_POST['trip-end'])) && 
            (!empty($_POST['trip-start'])) && 
            (!empty($_POST['renduLabel'])) && 
            (!empty($_POST['renduDescription'])) && 
            (isset($_POST['btnUpdRendu']))){ 

                $idBoiterendu = $_GET['id'];
                $label = htmlspecialchars($_POST['renduLabel']);
                $description = htmlspecialchars($_POST['renduDescription']);
                $idgroupe = htmlspecialchars($_POST['renduGroupe']);
                $datecreation = htmlspecialchars($_POST['trip-start']);
                $dateline = htmlspecialchars($_POST['trip-end']);
                $prof = $_SESSION['username'];

                updRendu($db,$idBoiterendu, $label, $description, $idgroupe, $prof, $datecreation, $dateline);
                
                $idBoiterendu = $_GET['id'];
                $OneBoiteRendu = getOneBoiteRendu($db, $idBoiterendu);

                echo $twig -> render('updRendu.html.twig', ["OneBoiteRendu" => $OneBoiteRendu, 'groupes' => getAllGroupes($db),]);

            }else{
                if(isset($_POST['btnUpdRendu'])==true){
                    echo '<script language="Javascript">
                        alert ("Veuillez remplir tous les champs" )
                        </script>';
                }
                $idBoiterendu = $_GET['id'];
                $OneBoiteRendu = getOneBoiteRendu($db, $idBoiterendu);
                echo $twig -> render('updRendu.html.twig', ["OneBoiteRendu" => $OneBoiteRendu, 'groupes' => getAllGroupes($db),]);
            }
        }else{

            $idBoiterendu = $_GET['id'];
            $OneBoiteRendu = getOneBoiteRendu($db, $idBoiterendu);

            #var_dump($OneBoiteRendu);

            echo $twig -> render('updRendu.html.twig', ["OneBoiteRendu" => $OneBoiteRendu, 'groupes' => getAllGroupes($db),
            ]);
        }
    }else{
        echo $twig->render("notFound.html.twig", []);
    }
    
}
