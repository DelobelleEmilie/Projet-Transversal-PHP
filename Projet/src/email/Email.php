<?php
use PHPMailer\PHPMailer\PHPMailer;

require_once __DIR__.'../../model/RenduModel.php';
require_once __DIR__.'../../model/EmailModel.php';
require_once __DIR__.'../../../vendor/autoload.php';
require_once __DIR__.'../../../config/config.php';
require_once __DIR__.'../../../src/database.php';

$db = getConnection($config);
if (!empty ($db)){
    $rendusferme = AfficherRenduBientotFerme($db);
    foreach ($rendusferme as $rendu){
        FermerRendu($db, $rendu['idboiterendu']);
        $email = new Mail();
        $email->envoyerMailer("antoine.bruye@epsi.fr", "A propos de votre boite de rendu, ".$rendu['prof'] , "Votre boîte de rendu ".$rendu['label']." est fermée!" );    
    }
    $rendusouvert = AfficherRenduBientotOuvert($db);
    foreach ($rendusouvert as $rendu){
        OuvrirRendu($db, $rendu['idboiterendu']);
    }
};
