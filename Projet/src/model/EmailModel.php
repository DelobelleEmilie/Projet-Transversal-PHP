<?php 
class Mail {
    //put your code here
    public function __construct() {
        }
    public function envoyerMailer($destinataire, $sujet, $message){
        $mail = new PHPMailer\PHPMailer\PHPMailer();
        $mail->isSMTP(); // Paramétrer le Mailer pour utiliser SMTP
        $mail->CharSet = 'UTF-8'; // Définir le charset
        $mail->Host = 'smtp.office365.com'; // Spécifier le serveur SMTP
        $mail->SMTPAuth = true; // Activer authentication SMTP
        $mail->Username = 'PLACEEHOLDER' ; // Votre adresse email d'envoi
        $mail->Password = 'PLACEHOLDER'; // Le mot de passe de cette adresse email
        $mail->SMTPSecure = 'tls'; // Accepter SSL
        $mail->Port = 587;
        $mail->setFrom('antoine.bruye@epsi.fr', 'Antoine'); // Personnaliser l'envoyeur
        $mail->addAddress($destinataire);
        $mail->isHTML(true); // Paramétrer le format des emails en HTML ou non
        $mail->Subject = $sujet;
        $mail->Body = $message;
        if(!$mail->send()) {
            echo 'Erreur, message non envoyé.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Le message a bien été envoyé !';
        }
    }
}
?>