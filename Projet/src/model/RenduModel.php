<?php


function getAllBoiteRendu($db, $idparent)
{
    $query = $db->prepare(" SELECT boite_rendu.idboiterendu, boite_rendu.label, boite_rendu.idgroupe, boite_rendu.prof, boite_rendu.datecreation, boite_rendu.dateline, boite_rendu.estOuvert

                            FROM boite_rendu
                            inner join groupe ON groupe.idgroupe = boite_rendu.idgroupe
                            WHERE groupe.idgroupe= :id;");
    $query->execute([
        'id' => $idparent
    ]);
    $boiterendu = $query->fetchAll();
    return $boiterendu;
}


function getOneBoiteRendu($db, $idBoiterendu)
{
    $query = $db->prepare(" SELECT boite_rendu.idboiterendu, boite_rendu.label, boite_rendu.description, boite_rendu.idgroupe, boite_rendu.datecreation, boite_rendu.dateline
                            FROM boite_rendu
                            WHERE boite_rendu.idboiterendu= :id;");
    $query->execute([
        'id' => $idBoiterendu
    ]);
    $boiterendu = $query->fetch();
    return $boiterendu;
}

function getAllUsersRendu($db, $idparent)
{
    $query = $db->prepare(" SELECT DISTINCT user.id, user.pseudo, devoir.idboiterendu
                            FROM user
                            JOIN devoir ON devoir.iduser = user.id AND devoir.idboiterendu = :idboiterendu;");
    $query->execute([
        'idboiterendu' => $idparent,
    ]);
    $usersrendu = $query->fetchAll();
    return $usersrendu;
}

function getOneUserRendu($db, $iduser)
{
    $query = $db->prepare(" SELECT user.id, user.pseudo
                            FROM user
                            WHERE user.id = :iduser ;");
    $query->execute([
        'iduser' => $iduser,
    ]);
    $user = $query->fetch();
    return $user;
}


function getAllDevoirBoiterendu($db, $idparent)
{
    $query = $db->prepare(" SELECT devoir.iddevoir, devoir.idboiterendu, devoir.iduser, devoir.nom_fichier, devoir.chemin_fichier
                            FROM devoir
                            WHERE devoir.idboiterendu = :idboiterendu;");
    $query->execute([
        'idboiterendu' => $idparent
    ]);
    $devoirsrendu = $query->fetchAll();
    return $devoirsrendu;
}

function getAllDevoirBoiterenduUser($db, $idboiterendu, $iduser){
    $query = $db->prepare(" SELECT devoir.iddevoir, devoir.idboiterendu, devoir.iduser, devoir.nom_fichier, devoir.chemin_fichier
                            FROM devoir
                            WHERE devoir.idboiterendu = :idboiterendu AND devoir.iduser = :iduser ;");
    $query->execute([
        'idboiterendu' => $idboiterendu,
        'iduser' => $iduser
    ]);
    $devoirsrendu = $query->fetchAll();
    return $devoirsrendu;
}

function getOneDevoir($db, $iddevoir)
{
    $query = $db->prepare(" SELECT devoir.iddevoir, devoir.idboiterendu, devoir.iduser, devoir.nom_fichier, devoir.chemin_fichier
                            FROM devoir
                            WHERE devoir.iddevoir = :iddevoir;");
    $query->execute([
        'iddevoir' => $iddevoir
    ]);
    $devoir = $query->fetch();
    return $devoir;
}



function getAllClasse($db)
{
    $query = $db->prepare(" SELECT idgroupe, label
                            FROM groupe
                            WHERE idgroupe != 3;");

    $query->execute([]);
    $classe = $query->fetchAll();
    return $classe;
}

function AfficherRenduBientotFerme($db)
{
    $query = $db->prepare("SELECT idboiterendu, label, `description`, idgroupe, prof, datecreation, dateline, estOuvert
                            FROM boite_rendu
                            WHERE estOuvert = 1 AND NOW() > dateline ");
    $query->execute([]);
    $rendus = $query->fetchAll();
    return $rendus;
}

function AfficherRenduBientotOuvert($db)
{
    $query = $db->prepare("SELECT idboiterendu, label, `description`, idgroupe, prof, datecreation, dateline, estOuvert
                            FROM boite_rendu
                            WHERE estOuvert = 0 AND NOW() BETWEEN datecreation and dateline ");
    $query->execute([]);
    $rendus = $query->fetchAll();
    return $rendus;
}

function FermerRendu($db, $idboiterendu)
{
    $query = $db->prepare("UPDATE boite_rendu 
                            SET estOuvert = 0
                            WHERE idboiterendu = :idboiterendu ");
    $query->execute([
        'idboiterendu' => $idboiterendu
    ]);
}

function OuvrirRendu($db, $idboiterendu)
{
    $query = $db->prepare("UPDATE boite_rendu 
                            SET estOuvert = 1
                            WHERE idboiterendu = :idboiterendu ");
    $query->execute([
        'idboiterendu' => $idboiterendu
    ]);
}

function AfficherRenduEleveOuvert($db, $pseudo)
{
    //On récup les groupes du user par le pseudo sous forme d'array
    $groupes = SQLArrayToArray(getUserGroupeFromPseudo($db, $pseudo));
    $rendus = [];

    //On récup ts les rendus pour chaque groupe
    foreach ($groupes as $groupe) {

        $query = $db->prepare("SELECT idboiterendu, label, boite_rendu.description, prof, datecreation, dateline, estOuvert
                            FROM boite_rendu 
                            WHERE idgroupe = :groupeUser AND estOuvert = 1");
        $query->execute([
            'groupeUser' => $groupe
        ]);
        $rendusUnGroupe = $query->fetchAll();

        //On met tout dans un seul array (évite les imbrications array dans array)
        foreach ($rendusUnGroupe as $unRendu) {
            array_push($rendus, $unRendu);
        }
    }

    // and NOW() BETWEEN boite_rendu.datecreation AND boite_rendu.dateline

    return $rendus;
}

function AfficherRenduEleveFerme($db, $pseudo)
{
    //On récup les groupes du user par le pseudo sous forme d'array
    $groupes = SQLArrayToArray(getUserGroupeFromPseudo($db, $pseudo));
    $rendus = [];

    //On récup ts les rendus pour chaque groupe
    foreach ($groupes as $groupe) {

        $query = $db->prepare("SELECT idboiterendu, label, boite_rendu.description, prof, datecreation, dateline, estOuvert
                            FROM boite_rendu 
                            WHERE idgroupe = :groupeUser and estOuvert = 0");
        $query->execute([
            'groupeUser' => $groupe
        ]);
        $rendusUnGroupe = $query->fetchAll();

        //On met tout dans un seul array (évite les imbrications array dans array)
        foreach ($rendusUnGroupe as $unRendu) {
            array_push($rendus, $unRendu);
        }
    }

    // and NOW() BETWEEN boite_rendu.datecreation AND boite_rendu.dateline

    return $rendus;
}

function saveRendu($db, $label, $description, $idgroupe, $prof, $datecreation, $dateline)
{
    $query = $db->prepare("INSERT INTO boite_rendu(label,`description`, idgroupe, prof, datecreation, dateline) 
                            VALUES (:label,:descr,:idgroupe, :prof , :datecreation, :dateline)");
    $query->execute([
        'label' => $label,
        'descr' => $description,
        'idgroupe' => $idgroupe,
        'prof' => $prof,
        'datecreation' => $datecreation,
        'dateline' => $dateline
    ]);
}

function updRendu($db, $idBoiterendu, $label, $description, $idgroupe, $prof, $datecreation, $dateline)
{
    $query = $db->prepare("UPDATE boite_rendu 
                            SET label = :label, `description`= :descr, idgroupe = :idgroupe, prof = :prof , datecreation = :datecreation, dateline = :dateline
                            WHERE idboiterendu = :idboiterendu ");
    $query->execute([
        'idboiterendu' => $idBoiterendu,
        'label' => $label,
        'descr' => $description,
        'idgroupe' => $idgroupe,
        'prof' => $prof,
        'datecreation' => $datecreation,
        'dateline' => $dateline
    ]);
}

function delDevoir($db, $id)
{
    $query = $db->prepare("DELETE FROM devoir
                            WHERE iddevoir = :iddevoir ");
    $query->execute([
        'iddevoir' => $id,
    ]);
}

function delRenduFromGroupe($db, $idGroupe)
{
    $idRendu = getRenduFromGroupe($db, $idGroupe)['idboiterendu'];

    $query = $db->prepare("SELECT devoir.chemin_fichier
                            FROM devoir
                            WHERE idboiterendu = :idboiterendu");
    $query -> execute([
        'idboiterendu' => $idRendu,
    ]);
    $fichiers = $query -> fetchAll();

    foreach($fichiers as $fichiersuppr){
        unlink($fichiersuppr['chemin_fichier']);
    }
    foreach($fichiers as $fichiersuppr){
        $dossier_user = dirname($fichiersuppr['chemin_fichier']);
        rmdir($dossier_user);
    }
    foreach($fichiers as $fichiersuppr){
        $dossier_user = dirname($fichiersuppr['chemin_fichier']);
        $dossier_boiterendu = dirname($dossier_user);
        rmdir($dossier_boiterendu);
    }
    
    $query = $db->prepare("DELETE FROM devoir
                            WHERE idboiterendu = :idboiterendu ");
    $query->execute([
        'idboiterendu' => $idRendu,
    ]);

    $query = $db->prepare("DELETE FROM boite_rendu
                            WHERE idgroupe = :idgroupe ");
    $query->execute([
        'idgroupe' => $idGroupe,
    ]);
    
}

function getRenduFromGroupe($db, $idGroupe)
{
    $query = $db->prepare(" SELECT boite_rendu.idboiterendu
                            FROM boite_rendu
                            WHERE idgroupe= :idGroupe;");
    $query->execute([
        'idGroupe' => $idGroupe
    ]);
    $boiterendu = $query->fetch();
    return $boiterendu;
}

function getAllRendu($db)
{ ##fonction avec un nom clair, recuperer un seul product et on recoit la base de donnees et id qu'on veut recup
    $query = $db->prepare("SELECT idboiterendu, label, prof ##query variable dans lequelle je vais faire une requete sql, preparer la requete sql, va stocker une requete SELECT selectionne les elements dans la base de données
                            FROM boite_rendu "); ## instruction quand id est egale aux idproducts, :id = peut etre modifier. les paramètres principaux SQL doivent etre en maj
    $query->execute([]);
    $rendu = $query->fetchAll(); ##recuperer les products, stoocker le resultat du query. que les resultats . plusieurs resultat = fetchAll
    return $rendu;
}

function addFichierRendu($db, $idboiterendu, $iduser, $nom_fichier, $chemin_fichier)
{
    $query = $db->prepare("INSERT INTO devoir(`idboiterendu`, `iduser`, `nom_fichier`, `chemin_fichier`)
                            VALUES (:idboiterendu, :iduser, :nom_fichier, :chemin_fichier)");
    $query->execute([
        'idboiterendu' => $idboiterendu,
        'iduser' => $iduser,
        'nom_fichier' => $nom_fichier,
        'chemin_fichier' => $chemin_fichier,
    ]);
}





function getAllGroupes($db)
{
    $query = $db->prepare("SELECT idgroupe, label
                            FROM groupe ");
    $query->execute([]);
    return $query->fetchAll();
}

function getOneGroupe($db, $idgroupe)
{
    $query = $db->prepare("SELECT idgroupe, label
                            FROM groupe
                            WHERE idgroupe = :idgroupe ");
    $query->execute([
        'idgroupe' => $idgroupe
    ]);
    return $query->fetch();
}


function getOneGroupeFromLabel($db, $label)
{
    $query = $db->prepare("SELECT idgroupe, label
                            FROM groupe
                            WHERE label = :label ");
    $query->execute([
        'label' => $label
    ]);
    return $query->fetch();
}

function getAllUsersSansGroupe($db)
{
    $query = $db->prepare("SELECT id, pseudo, groupe FROM user WHERE groupe = 0 ");
    $query->execute([]);
    $users = $query->fetchAll();
    return $users;
}

function getUserGroupeFromPseudo($db, $pseudo)
{
    $query = $db->prepare(" SELECT pseudo, groupe, id
                            FROM user
                            WHERE pseudo = :pseudo;");
    $query->execute([
        'pseudo' => $pseudo
    ]);
    $user = $query->fetch();
    return $user['groupe'];
}

function saveGroupe($db, $label, $users)
{

    if ($label != "") {

        $query = $db->prepare("SELECT label FROM groupe WHERE label = :label");
        $query->execute([
            'label' => $label
        ]);

        if ($query->fetch() == false) {

            //On ajoute le groupe
            $query = $db->prepare("INSERT INTO groupe(label) VALUES (:label)");
            $query->execute([
                'label' => $label
            ]);

            //On recup l'id du groupe ajouté
            $query = $db->prepare("SELECT idgroupe FROM groupe WHERE label = :label");
            $query->execute([
                'label' => $label
            ]);
            $groupe = $query->fetch();

            for ($i = 0; $i < count($users); $i++) {
                $pseudo = $users[$i];

                $userGroupe = getUserGroupeFromPseudo($db, $pseudo);

                $userGroupe = pushToSQLArray($userGroupe, $groupe[0]);

                $query = $db->prepare("UPDATE user SET groupe = :groupe WHERE pseudo = :pseudo ");
                $query->execute([
                    'groupe' => $userGroupe,
                    'pseudo' => $pseudo
                ]);
            }

            return "Groupe créé !";
        } else return "Le groupe existe déjà !";
    } else return "Veuillez entrer un nom !";
}

function delGroupe($db, $label)
{
    $idGroupe = getOneGroupeFromLabel($db, $label)['idgroupe'];

    $users = getAllUsers($db);

    foreach ($users as $user) {
        $groupes = $user['groupe'];
        if (in_array($idGroupe, $groupes)) {
            unset($groupes[array_search($idGroupe, $groupes)]);


            $groupes = ArrayToSQLArray($groupes);

            updateGroupeUser($db, $user['id'], $groupes);
        }
    }

    delRenduFromGroupe($db, $idGroupe);

    
    $query = $db->prepare("DELETE FROM groupe
                            WHERE label = :label");
    $query->execute([
        'label' => $label
    ]);
    
}

function updateGroupeUser($db, $id, $newGroupes)
{

    $query = $db->prepare("UPDATE user
                            SET groupe = :newGroupes
                            WHERE id = :id");
    $query->execute([
        'newGroupes' => $newGroupes,
        'id' => $id
    ]);
}



function addUser($db, $pseudo, $mdp, $isAdmin, $groupe)
{
    if ($pseudo != "" && $mdp != "") {
        #Verif que l'U n'existe pas
        $query = $db->prepare("SELECT pseudo FROM user WHERE pseudo = :pseudo");
        $query->execute([
            'pseudo' => $pseudo
        ]);

        if ($query->fetch() == false) {
            $query = $db->prepare("INSERT INTO user(pseudo, mdp, isAdmin, groupe) VALUES (:pseudo, :mdp, :isAdmin, :groupe)");
            $query->execute([
                'pseudo' => $pseudo,
                'mdp' => $mdp,
                'isAdmin' => $isAdmin,
                'groupe' => ArrayToSQLArray($groupe),
            ]);
        } else return "L'utilisateur existe déjà !";
    } else return "Veuillez entrer un pseudo et un mot de passe !";
}

function delUser($db, $pseudo)
{
    $query = $db->prepare("DELETE FROM user
                            WHERE pseudo = :pseudo");
    $query->execute([
        'pseudo' => $pseudo
    ]);
}

function getAllUsers($db)
{
    $query = $db->prepare("SELECT id, pseudo, groupe, isAdmin FROM user");
    $query->execute([]);
    $users = $query->fetchAll();
    for ($i = 0; $i < count($users); $i++) {

        $users[$i]['groupe'] = SQLArrayToArray($users[$i]['groupe']);
        $users[$i][2] = SQLArrayToArray($users[$i][2]);
    }

    return $users;
}




function getFichiersRendus($db)
{
    $query = $db->prepare("SELECT devoir.iddevoir, devoir.idboiterendu, devoir.iduser, devoir.nom_fichier, devoir.chemin_fichier
                            FROM devoir");
    $query->execute([]);
    $fichiersrendus = $query->fetchAll();
    return $fichiersrendus;
}

function delFichier($db, $iddevoir)
{
    $query = $db->prepare("DELETE FROM devoir
                            WHERE iddevoir = :iddevoir");
    $query->execute([
        'iddevoir' => $iddevoir
    ]);
}


function SQLArrayToArray($array)
{
    $res = explode('/', $array);
    if (count($res) == 0) $res = [intval($array)];
    return $res;
}

function pushToSQLArray($array1, $val)
{
    return $array1 . '/' . strval($val);
}

function ArrayToSQLArray($array)
{
    $res = "";
    if (is_array($array)) {
        foreach ($array as $case) {
            if ($res == "") {
                $res = strval($case);
            } else {
                $res = $res . '/' . strval($case);
            }
        }
        return $res;
    }

    return $array;
}
