<?php

function Authentification($db)
{
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $isAdmin = 0;

        $query = $db->prepare("SELECT id, pseudo, mdp, isAdmin, groupe FROM user WHERE pseudo=:username");
        $query->execute([
            'username' => $username,
        ]);

        $user = $query->fetch();


        if (isset($user[0]) && $user[0] != null && password_verify($password, $user[2])) {
            $_SESSION['iduser'] = $user[0];
            $_SESSION['username'] = $user[1];
            $_SESSION['password'] = $user[2];
            $_SESSION['isAdmin']  = $user[3];
            $_SESSION['idgroupe'] = $user[4];
        } else {
            $_POST["mdpError"] = true;
            return "faux mdp";
        }

        return "connected";
    } else {
        return "not connected";
    }
}

function getcountIdUser($db,$user){
    $query = $db->prepare("     SELECT count(*) 
                                FROM user 
                                WHERE pseudo = :pseudo;");
    $query->execute([
            'pseudo' => $user]);
    $user = $query->fetch();
    return $user;
    
}

function getiduser($db,$user){
    $query = $db->prepare("     SELECT id 
                                FROM user 
                                WHERE pseudo = :pseudo ;");
    $query->execute([
            'pseudo' => $user]);
    $user = $query->fetch();
    return $user["id"];   
}

function updatemdp($db,$id,$newpassword){
    $query = $db -> prepare("   UPDATE user
                                SET mdp = :newpassword
                                WHERE id = :iduser ;");
    $query -> execute([
    'iduser' => $id , 
    'newpassword' => $newpassword ]);
}